import os
import pickle
import numpy as np
from collections import Counter
from gensim.models import Word2Vec
import torch
import torch.nn as nn
from torch.utils.data import Dataset
from torch.utils.data import DataLoader
from tensorboardX import SummaryWriter


train_on_gpu = torch.cuda.is_available()
if not train_on_gpu:
    print('No GPU found. Training will happen on CPU and may take a long time.')


class Vocabulary(object):

    def __init__(self):
        self._word2idx = {}
        self._idx2word = {}
        self._counter = Counter()
        self._size = 0
        self._punctuation2token = {';': "<semicolon>",
                                   ':': "<colon>",
                                   "'": "<inverted_comma>",
                                   '"': "<quotation_mark>",
                                   ',': "<comma>",
                                   '\n': "<new_line>",
                                   '!': "<exclamation_mark>",
                                   '-': "<hyphen>",
                                   '--': "<hyphens>",
                                   '.': "<period>",
                                   '?': "<question_mark>",
                                   '(': "<left_paren>",
                                   ')': "<right_paren>",
                                   '♪': "<music_note>",
                                   '[': "<left_square>",
                                   ']': "<right_square>",
                                   "’": "<inverted_comma>",
                                   }
        self.add_text('<pad>')
        self.add_text('<unknown>')

    def add_word(self, word):
        """
        Adds a token to the vocabulary
        :param word: (str) word to add to vocabulary
        :return: None
        """
        word = word.lower()
        if word not in self._word2idx:
            self._idx2word[self._size] = word
            self._word2idx[word] = self._size
            self._size += 1
        self._counter[word] += 1

    def add_text(self, text):
        """
        Splits text into tokens and adds to the vocabulary
        :param text: (str) text to add to vocabulary
        :return: None
        """
        text = self.clean_text(text)
        tokens = self.tokenize(text)
        for token in tokens:
            self.add_word(token)

    def clean_text(self, text):
        """
        Cleans text for processing
        :param text: (str) text to be cleaned
        :return: (str) cleaned text
        """
        text = text.lower().strip()
        for key, token in self._punctuation2token.items():
            text = text.replace(key, ' {} '.format(token))
        text = text.strip()
        while '  ' in text:
            text = text.replace('  ', ' ')
        return text

    def tokenize(self, text):
        """
        Splits text into individual tokens
        :param text: (str) text to be tokenized
        :return: (list) list of tokens in text
        """
        return text.split(' ')

    def set_vocab(self, vocab):
        self._word2idx = {}
        self._idx2word = {}
        self._counter = Counter()
        self._size = 0
        self.add_text('<pad>')
        self.add_text('<unknown>')
        for word in vocab:
            self.add_word(word)

    def most_common(self, n):
        """
        Creates a new vocabulary object containing the n most frequent tokens from current vocabulary
        :param n: (int) number of most frequent tokens to keep
        :return: (Vocabulary) vocabulary containing n most frequent tokens
        """
        tmp = Vocabulary()
        for w in self._counter.most_common(n):
            tmp.add_word(w[0])
            tmp._counter[w[0]] = w[1]
        return tmp

    def load(self, path='vocab.pkl'):
        """
        Loads vocabulary from given path
        :param path: (str) path to pkl object
        :return: None
        """
        with open(path, 'rb') as f:
            self.__dict__.clear()
            self.__dict__.update(pickle.load(f))
        print("\nVocabulary successfully loaded from [{}]\n".format(path))

    def add_punctuation(self, text):
        """
        Replces punctuation tokens with corresponding characters
        :param text: (str) text to process
        :return: text with punctuation tokens replaced with characters
        """
        for key, token in self._punctuation2token.items():
            text = text.replace(token, ' {} '.format(key))
        text = text.strip()
        while '  ' in text:
            text = text.replace('  ', ' ')
        text = text.replace(' :', ':')
        text = text.replace(" ' ", "'")
        text = text.replace("[ ", "[")
        text = text.replace(" ]", "]")
        text = text.replace(" .", ".")
        text = text.replace(" ,", ",")
        text = text.replace(" !", "!")
        text = text.replace(" ?", "?")
        text = text.replace(" ’ ", "’")
        return text

    def __len__(self):
        """
        Number of unique words in vocabulary
        """
        return self._size

    def __str__(self):
        s = "Vocabulary contains {} tokens\nMost frequent tokens:\n".format(
            self._size)
        for w in self._counter.most_common(10):
            s += "{} : {}\n".format(w[0], w[1])
        return s

    def __getitem__(self, item):
        """
        Returns the word corresponding to an id or and id corresponding to a word in the vocabulary.
        Return <unknown> if id/word is not present in the vocabulary
        """
        if isinstance(item, int):
            return self._idx2word[item]
        elif isinstance(item, str):
            if item in self._word2idx:
                return self._word2idx[item]
            else:
                return self._word2idx['<unknown>']
        return None


embeddings = np.load('../input/embeddings.npy')


class MortyFire(nn.Module):

    def __init__(self, vocab_size, embed_size, lstm_size, seq_length, num_layers, dropout=0.5, bidirectional=False, train_on_gpu=True, embeddings=None):
        nn.Module.__init__(self)
        self.vocab_size = vocab_size
        self.num_layers = num_layers
        self.lstm_size = lstm_size
        self.seq_length = seq_length
        self.embed_size = embed_size
        self.train_on_gpu = train_on_gpu
        self.bidirectional = bidirectional

        self.embedding = nn.Embedding(vocab_size, embed_size)
        if embeddings is not None:
            self.embedding.weight = nn.Parameter(torch.from_numpy(embeddings))
            self.embedding.weight.requires_grad = False
        self.lstm = nn.LSTM(embed_size, lstm_size, num_layers,
                            dropout=dropout, batch_first=True, bidirectional=bidirectional)
        self.dropout = nn.Dropout(dropout)
        self.fc = nn.Linear(lstm_size * 2, vocab_size)

    def forward(self, batch, hidden):
        batch_size = batch.size(0)
        embeds = self.embedding(batch)
        lstm_out, hidden = self.lstm(embeds, hidden)
        lstm_out = lstm_out.contiguous().view(-1, self.lstm_size * 2)
        drop = self.dropout(lstm_out)
        output = self.fc(drop)
        output = output.view(batch_size, -1, self.vocab_size)
        out = output[:, -1]
        return out, hidden

    def init_hidden(self, batch_size):
        weight = next(self.parameters()).data
        layers = self.num_layers if not self.bidirectional else self.num_layers * 2
        if self.train_on_gpu:
            hidden = (weight.new(layers, batch_size, self.lstm_size).zero_().cuda(),
                      weight.new(layers, batch_size, self.lstm_size).zero_().cuda())
        else:
            hidden = (weight.new(layers, batch_size, self.lstm_size).zero_(),
                      weight.new(layers, batch_size, self.lstm_size).zero_())
        return hidden


class RickAndMortyData(Dataset):

    """ Wrapper class to process and produce training samples """

    def __init__(self, text, seq_length, vocab=None):
        self.text = text
        self.seq_length = seq_length
        if vocab is None:
            self.vocab = Vocabulary()
            self.vocab.add_text(self.text)
        else:
            self.vocab = vocab

        self.text = self.vocab.clean_text(text)
        self.tokens = self.vocab.tokenize(self.text)

    def __len__(self):
        return len(self.tokens) - self.seq_length

    def __getitem__(self, idx):
        x = [self.vocab[word]
             for word in self.tokens[idx:idx + self.seq_length]]
        y = [self.vocab[self.tokens[idx + self.seq_length]]]
        x = torch.LongTensor(x)
        y = torch.LongTensor(y)
        return x, y


def _pick_word(probabilities, temperature):
    """
    Pick the next word in the generated text
    :param probabilities: Probabilites of the next word
    :return: String of the predicted word
    """
    probabilities = np.log(probabilities) / temperature
    exp_probs = np.exp(probabilities)
    probabilities = exp_probs / np.sum(exp_probs)
    pick = np.random.choice(len(probabilities), p=probabilities)
    while int(pick) == 1:
        pick = np.random.choice(len(probabilities), p=probabilities)
    return pick


def generate(model, start_seq, vocab, length=100, temperature=1.0):
    model.eval()

    tokens = vocab.clean_text(start_seq)
    tokens = vocab.tokenize(tokens)
    # create a sequence (batch_size=1) with the prime_id
    current_seq = np.full((1, model.seq_length), vocab['<pad>'])
    for idx, token in enumerate(tokens):
        current_seq[-1][idx - len(tokens)] = vocab[token]
        predicted = tokens

    for _ in range(length):
        if train_on_gpu:
            current_seq = torch.LongTensor(current_seq).cuda()
        else:
            current_seq = torch.LongTensor(current_seq)

        hidden = model.init_hidden(current_seq.size(0))
        output, _ = model(current_seq, hidden)
        p = torch.nn.functional.softmax(output, dim=1).data
        if train_on_gpu:
            p = p.cpu()

        probabilities = p.numpy().squeeze()
        word_i = _pick_word(probabilities, temperature)
    # retrieve that word from the dictionary
        word = vocab[int(word_i)]
        predicted.append(word)
    # the generated word becomes the next "current sequence" and the cycle can continue
        current_seq = current_seq.cpu().data.numpy()
        current_seq = np.roll(current_seq, -1, 1)
        current_seq[-1][-1] = word_i
        gen_sentences = ' '.join(predicted)

    gen_sentences = vocab.add_punctuation(gen_sentences)
    return gen_sentences


lstm_size = 256
seq_length = 20
num_layers = 2
bidirectional = False
embeddings_size = 300
dropout = 0.5


vocab = Vocabulary()
# vocab.add_text(text)
vocab.load('../input/vocab.pkl')


model = MortyFire(vocab_size=len(vocab), lstm_size=lstm_size, embed_size=embeddings_size, seq_length=seq_length,
                  num_layers=num_layers, dropout=dropout, bidirectional=bidirectional, train_on_gpu=train_on_gpu, embeddings=embeddings)
if train_on_gpu:
    model.cuda()
print(model)


model_path = '../input/mortyfire'
model.load_state_dict(torch.load(model_path))
start_sequence = "rick: (burps)"
temperature = 0.8
script_length = 1000
script = generate(model, start_seq=start_sequence, vocab=vocab,
                  temperature=temperature, length=script_length)
print('----- Temperatue: {} -----'.format(temperature))
print(script)
